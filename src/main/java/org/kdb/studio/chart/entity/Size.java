package org.kdb.studio.chart.entity;

public class Size implements Overridable<Size> {
    public int width;
    public int height;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void override(Size obj) {
        Overridable.overrideObject(this, obj);
    }
}
