package org.kdb.studio.chart;

public enum PlotDefaultType {
    XYLineChart, TimeSeriesChart
}
